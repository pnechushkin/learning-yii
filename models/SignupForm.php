<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $mail
 * @property string $role
 */
class SignupForm extends \yii\db\ActiveRecord
{


	public $login;
	public $password;
	public $mail;

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'mail'], 'required'],
            [['login'], 'string', 'max' => 12],
            [['password', 'mail', 'role'], 'string', 'max' => 255],
            [['login'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Password',
            'mail' => 'Mail',
            'role' => 'Role',
        ];
    }
}
