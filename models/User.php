<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    protected $id;
//	protected $login;
//	protected $password;
//	protected $mail;
//	protected $authKey;
//	protected $accessToken;
//	protected $auth_key;



//    private static $users = self::findAll();

	public static function tableName()
	{
		return 'users';
	}
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
	    return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
	    return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
//    	return static::wher
//        foreach (self::$users as $user) {
//            if (strcasecmp($user['login'], $username) === 0) {
//                return new static($user);
//            }
//        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey=== $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
//    	\Yii::$app->security->generateRandomString()
        return $this->password === $password;
    }


	/**
	 * @param mixed $username
	 */
	public function setUsername( $username ) {
		$this->login = $username;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword( $password ) {
		$this->password = $password;
	}

	/**
	 * @param mixed $mail
	 */
	public function setMail( $mail ) {
		$this->mail = $mail;
	}

	/**
	 * @param mixed $authKey
	 */
	public function setAuthKey( $authKey ) {
		$this->authKey = $authKey;
	}

	/**
	 * @param mixed $accessToken
	 */
	public function setAccessToken( $accessToken ) {
		$this->accessToken = $accessToken;
	}

	/**
	 * @return mixed
	 */
	public function getUsername() {
		return $this->login;
	}

	/**
	 * @return mixed
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @return mixed
	 */
	public function getMail() {
		return $this->mail;
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->auth_key = \Yii::$app->security->generateRandomString();
			}
			return true;
		}
		return false;
	}



}
