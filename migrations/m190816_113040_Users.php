<?php

use yii\db\Migration;

/**
 * Class m190816_113040_Users
 */
class m190816_113040_Users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('users', [
		    'id' => $this->primaryKey(),
		    'login' => $this->string(12)->notNull()->unique(),
		    'password' => $this->string()->notNull(),
		    'mail' => $this->string()->notNull(),
		    'auth_key' => $this->string(),
		    'role' => $this->string()->defaultValue('user'),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190816_113040_Users cannot be reverted.\n";

        return false;
    }
    */
}
